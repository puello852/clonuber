import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert } from 'ionic-angular';
import { Http } from '@angular/http'
import { AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AjustesService } from '../../providers/ajustes/ajustes'

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {
    Nombre: any;
    Apellido: any;
    Correo: any;
    Opcion: any;
    id;
    Opcion2: any;
    dato: any;
    Contrasena: any; 
    mostrar = false
    trabajador = []
    Myform:FormGroup;
    formularioUsuario:FormGroup;
    mostrarTrabajador: boolean = true
    mostrarUsario: boolean = true
    constructor( public navCtrl: NavController, public navParams: NavParams,
       public http:Http,public alertCtrl: AlertController,
       public fb: FormBuilder,public loadingCtrl: LoadingController,private _ajustes:AjustesService) {
        this.buildForm();
        
    }

    
    
  

    buildForm() {
      
      this.formularioUsuario = this.fb.group({
        nombre:['',[Validators.required,Validators.minLength(3),Validators.maxLength(30)]],
        direccion:['',[Validators.required,Validators.minLength(3),Validators.maxLength(100)]],
        correo:['',[Validators.required,Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
        tipo_contacto:['Telefono',[Validators.required]],
        numero_contacto:['',[Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,20}/)]]
      });

      this.Myform = this.fb.group({
        nombre:['',[Validators.required,Validators.minLength(3),Validators.maxLength(30)]],
        direccion:['',[Validators.required,Validators.minLength(3),Validators.maxLength(100)]],
        correo:['',[Validators.required,Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
        tipo_contacto:['Telefono',[Validators.required]],
        numero_contacto:['',[Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,20}/)]]
      });
    }

    ionViewDidLoad() {

      if ( this._ajustes.ajustes.rol == 1 ){
        console.log("usuario")
        this.mostrarUsario = true
        this.mostrarTrabajador = false
      }
      
      else if (this._ajustes.ajustes.rol == 2){
        console.log("trabjador")
        this.mostrarUsario = false
        this.mostrarTrabajador = true
      }
     
        this.http.get("https://panes-uber.000webhostapp.com/trabajador/obtener_profesion.php")
        .subscribe(data=>{
          let datos = data["_body"]
          let profesion = JSON.parse(datos)
          this.trabajador = profesion
            console.log(this.trabajador)
        })
    }
    
    goregistro(){
      
      let loading = this.loadingCtrl.create({
         content: "Espere por favor..."
       });
  
       loading.present();
       
      if(this._ajustes.ajustes.rol == 2){
        let data = {
          "nombre": this.Nombre,
          "apellido": this.Apellido,
          "usuario": this.Correo,
          "passw": this.Contrasena,
          "profesion" : this.id,
          "id_rol" : 2
         } 
         console.log(JSON.stringify(data)    )

       this.http.post("https://panes-uber.000webhostapp.com/trabajador/insertar_trabajador.php", JSON.stringify(data))
        .subscribe(data=>{
          let body = data['_body']
          console.log(body.mensaje)
          const alert = this.alertCtrl.create({
            title: 'Exito',
            subTitle: 'Usuario creado correctamente',
            buttons: ['OK']
          });
          
          alert.present();
          loading.dismiss();
          this.navCtrl.setRoot(HomePage)
            
      },err=>{
          
          let doby = err['_body']             
          let estado = JSON.parse(doby)
            const alert = this.alertCtrl.create({
              title: 'Error',
              subTitle: estado.mensaje,
              buttons: ['OK']
            });
            alert.present();    
            loading.dismiss();       
        })
      }

      else{

        let data = {
         "nombre": this.Nombre,
         "apellido": this.Apellido,
         "usuario": this.Correo,
         "passw": this.Contrasena,
         "id_rol" : 1
        } 
  
        this.http.post("https://panes-uber.000webhostapp.com/clientes/insertar_cliente.php", JSON.stringify(data))
            .subscribe(data=>{
               
                
                let body = data['_body']
                console.log(body.mensaje)
                const alert = this.alertCtrl.create({
                  title: 'Exito',
                  subTitle: 'Usuario creado correctamente',
                  buttons: ['OK']
                });
                
                alert.present();
                loading.dismiss();
                this.navCtrl.setRoot(HomePage)
                  
            },err=>{
                
                let doby = err['_body']             
                let estado = JSON.parse(doby)
                  const alert = this.alertCtrl.create({
                    title: 'Error',
                    subTitle: estado.mensaje,
                    buttons: ['OK']
                  });
                  alert.present();       
                  loading.dismiss();       
            })
      }


      
  
  }
}
