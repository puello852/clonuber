import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccesLoginPage } from './succes-login';
import { AgmCoreModule } from '@agm/core';
@NgModule({
  declarations: [
    SuccesLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(SuccesLoginPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA-HXVa2jtkGfKtIJwisxgC46RaWqC1xuI'
    })
  ],
})
export class SuccesLoginPageModule {}
