import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController  } from 'ionic-angular';
import { HomePage } from '../home/home';
import { UbicacionProvider } from '../../providers/ubicacion/ubicacion'
import { AjustesService } from '../../providers/ajustes/ajustes';
import {Geolocation }from '@ionic-native/geolocation'; 

@IonicPage()
@Component({
  selector: 'page-succes-login',
  templateUrl: 'succes-login.html',
})
export class SuccesLoginPage {
  
  lat: number
  lng: number

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuctr:MenuController,
    public proveedor:UbicacionProvider,private _ajustes:AjustesService,
    private geolocation:Geolocation) {
      this.obtener();     
    }
    
    ionViewDidLoad() {
      /*this._ajustes.cargar_storage();
      this.lat = this._ajustes.ajustes.lat
      this.lng = this._ajustes.ajustes.lng
     console.log(this._ajustes.ajustes.lat) 
     console.log(this._ajustes.ajustes.lng)*/
    }

    
    
  openMenu(){
    this.menuctr.toggle();
    
  }

  cerrar(){
    this.navCtrl.setRoot(HomePage)
  }

  obtener() {
    this.geolocation.getCurrentPosition().then((resp) =>  {
   
    
    this.lat = resp.coords.latitude
    this.lng = resp.coords.longitude

    
    let watch = this.geolocation.watchPosition(); 
    watch.subscribe((data) =>  {
    
    
    this.lat = data.coords.latitude
    this.lng = data.coords.longitude
    
    }); 
    
    
    
    
    
    }).catch((error) =>  {
    console.log('Error getting location', error); 
    }); 
    }
    
    

  

  

}
