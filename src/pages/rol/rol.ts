import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AjustesService } from '../../providers/ajustes/ajustes'
import { RegistroPage } from '../registro/registro';
/**
 * Generated class for the RolPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rol',
  templateUrl: 'rol.html',
})
export class RolPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private _ajustes:AjustesService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RolPage');
  }

  persona(){

    this._ajustes.ajustes.rol = 1
    this._ajustes.guardar_storage()
    this.navCtrl.push(RegistroPage)
  }

  trabajador(){
    this._ajustes.ajustes.rol = 2
    this._ajustes.guardar_storage()
    this.navCtrl.push(RegistroPage)
  }

}
