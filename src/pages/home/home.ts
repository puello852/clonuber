import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegistroPage } from '../registro/registro';
import { LoginPage } from '../login/login';
import { RolPage } from '../rol/rol';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goregistro(){
    this.navCtrl.push(RolPage)
  }
  gologin(){
    this.navCtrl.push(LoginPage)
  }

}
