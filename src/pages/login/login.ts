import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Http } from '@angular/http'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SuccesLoginPage } from '../succes-login/succes-login';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  formularioUsuario:FormGroup;
  Correo:any
  Contrasena:any
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public http:Http, public fb: FormBuilder,public loadingCtrl: LoadingController) {
      this.buildForm();
  }

  buildForm() {
      
    this.formularioUsuario = this.fb.group({
      correo:['',[Validators.required,Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      password:['',[Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,20}/)]],
      
    });
  }

  

  gologin(){

    let loading = this.loadingCtrl.create({
      content: "Espere por favor..."
    });

    loading.present();

    let data = {
      "usuario": this.Correo,
      "passw": this.Contrasena
     } 

    

     console.log(data)
     
     this.http.post("https://panes-uber.000webhostapp.com/clientes/loggin_cliente2.php", JSON.stringify(data))
          .subscribe(data=>{
            console.log(data)
            loading.dismiss();
            this.navCtrl.setRoot(SuccesLoginPage)
          },err=>{
            console.log(err)
          })

  }

}
