import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { RegistroPageModule } from '../pages/registro/registro.module'
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPageModule } from '../pages/login/login.module'
import { HttpModule } from '@angular/http';
import { firebaseConfig } from '../conifg/firebase.config'
import { AngularFireModule } from 'angularfire2'
import { AngularFireAuthModule } from 'angularfire2/auth'
import {CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SuccesLoginPageModule } from '../pages/succes-login/succes-login.module';
import { RolPageModule } from '../pages/rol/rol.module'
import { IonicStorageModule } from '@ionic/storage';
import { AjustesService } from '../providers/ajustes/ajustes';
import { Geolocation } from '@ionic-native/geolocation';
import { UbicacionProvider } from '../providers/ubicacion/ubicacion';
@NgModule({
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    RegistroPageModule,
    LoginPageModule,
    SuccesLoginPageModule,
    HttpModule,
    AngularFireAuthModule,
    RolPageModule,
    IonicModule.forRoot(MyApp,{
      scrollAssist: false
    }),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AjustesService,
    Geolocation,
    UbicacionProvider
  ]
})
export class AppModule {}
