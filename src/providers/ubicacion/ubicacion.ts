import {Injectable }from '@angular/core'; 
import {Geolocation }from '@ionic-native/geolocation'; 
import { AjustesService } from '../ajustes/ajustes';

@Injectable()
export class UbicacionProvider {

constructor(private geolocation:Geolocation,private _ajustes:AjustesService) {
console.log('Hello UbicacionProvider Provider'); 
}

obtener() {
this.geolocation.getCurrentPosition().then((resp) =>  {
// resp.coords.latitude
// resp.coords.longitude

this._ajustes.ajustes.lat = resp.coords.latitude
this._ajustes.ajustes.lng = resp.coords.longitude
this._ajustes.guardar_storage();

let watch = this.geolocation.watchPosition(); 
watch.subscribe((data) =>  {
// data can be a set of coordinates, or an error (if an error occurred).
// data.coords.latitude
// data.coords.longitude

this._ajustes.ajustes.lat = data.coords.latitude
this._ajustes.ajustes.lng = data.coords.longitude
this._ajustes.guardar_storage();
}); 





}).catch((error) =>  {
console.log('Error getting location', error); 
}); 
}

}
