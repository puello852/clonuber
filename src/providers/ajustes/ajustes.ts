import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
@Injectable()
export class AjustesService {
  
  ajustes = {
    rol: 1,
    lng: 0,
    lat: 0,
  }
  constructor(private Plat: Platform,private storage: Storage) {
    console.log('Hello AjustesProvider Provider');
    

  }

  cargar_storage(){
  

    let promesa = new Promise ((resolve,reject)=>{

      if (this.Plat.is("cordova")){
        //dispositvo

        this.storage.ready().then (()=>{
          this.storage.get("ajustes")
          .then(ajustes=>{
            
            if(ajustes){
              this.ajustes = ajustes;
            }

            resolve();
          })
        })
      }else{
        //escritorio
       if (localStorage.getItem("ajustes")){
         this.ajustes =  JSON.parse(localStorage.getItem("ajustes")) 
       }
       resolve();
      }

    });

    return promesa;
    
  }

  guardar_storage(){


      
   if (this.Plat.is("cordova")){
      //dispositvo
      
      this.storage.ready()
        .then(()=>{

        this.storage.set("ajustes", this.ajustes)

      })

    }else{
      //escritorio
      localStorage.setItem("ajustes", JSON.stringify(this.ajustes))
    }
  }

}
